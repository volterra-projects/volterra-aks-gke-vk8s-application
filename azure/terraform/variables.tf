variable "resource_prefix" {
  description = "The prefix for all the created objects, e.g. '<YOURINITIALS>-voltlab'"

  validation {
    condition     = length(var.resource_prefix) > 0 && length(var.resource_prefix) < 13
    error_message = "The resource prefix must not be empty and must be less than 13 characters."
  }
}

variable "cluster_name" {
  default     = "aks-cluster"
  description = "Kubernetes cluster name"
}

variable "location" {
  default     = "northeurope"
  description = "The Azure Region in which all resources in this example should be provisioned"
}

variable "vm_size" {
  default     = "Standard_D4a_v4"
  description = "4 vpus, 16 GiB memory"
}

variable "tag_owner" {
  description = "A tag attached to all objects containing the name of the owner, e.g. 'John Smith'"

  validation {
    condition     = length(var.tag_owner) > 0
    error_message = "The owner tag must not be empty."
  }
}

variable "aks_version_prefix" {
  type        = string
  description = "Kubernetes version prefix to use in AKS"
  default     = "1.21"
}

variable "api_cert_path" {
  type        = string
  description = "Volterra API Cert path"
}

variable "api_key_path" {
  type        = string
  description = "Volterra API private key path"
}

variable "api_url" {
  type        = string
  description = "Volterra tenant api url"
}

variable "subnet_inside_name" {
  type        = string
  description = "inside subnet name"
  default     = "subnet-inside"
}

variable "subnet_outside_name" {
  type        = string
  description = "inside subnet name"
  default     = "subnet-outside"
}
