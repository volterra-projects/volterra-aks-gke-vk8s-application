variable "api_cert_path" {
  type        = string
  description = "Volterra API Cert path"
}

variable "api_key_path" {
  type        = string
  description = "Volterra API private key path"
}

variable "api_url" {
  type        = string
  description = "Volterra tenant api url"
}

variable "prefix" {
  type        = string
  description = "Object Prefix"
}

variable "location" {

}

variable "vnet_name" {

}

variable "vnet_resource_group_name" {

}

variable "vnet_subnet_inside_name" {

}

variable "create_k8s_service_discorvery" {
  type    = bool
  default = false
}

variable "kubeconfig_raw" {
  default = ""
}

variable "tag_owner" {

}

variable "ce_site_selector" {
  type = string
}

variable "timeout" {
  default = null
}
