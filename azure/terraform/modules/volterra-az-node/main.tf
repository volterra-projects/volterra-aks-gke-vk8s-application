terraform {
  required_providers {
    volterra = {
      source  = "volterraedge/volterra"
      version = "~> 0.11.0"
    }
  }
}

provider "volterra" {
  api_cert = var.api_cert_path
  api_key  = var.api_key_path
  url      = var.api_url
}

# resource "volterra_cloud_credential" "name" {

# }

resource "volterra_azure_vnet_site" "az_node" {
  lifecycle {
    ignore_changes = [labels]
  }
  name      = trim("${var.vnet_name}", "-")
  namespace = "system"

  // One of the arguments from this list "azure_cred assisted" must be set
  azure_cred {
    name      = "azure-ne"
    namespace = "system"
  }

  tags = {
    "Owner" = var.tag_owner
  }

  // One of the arguments from this list "logs_streaming_disabled log_receiver" must be set
  logs_streaming_disabled = true
  // One of the arguments from this list "azure_region alternate_region" must be set
  azure_region   = var.location
  resource_group = "${var.vnet_name}-F5XC-node-resources"

  // One of the arguments from this list "ingress_gw ingress_egress_gw voltstack_cluster ingress_gw_ar ingress_egress_gw_ar voltstack_cluster_ar" must be set
  ingress_gw {
    az_nodes {
      azure_az = "1"
      //disk_size = "disk_size"

      local_subnet {
        // One of the arguments from this list "subnet_param subnet" must be set
        subnet {
          subnet_name         = var.vnet_subnet_inside_name
          vnet_resource_group = true
        }
      }
    }
    azure_certified_hw = "azure-byol-voltmesh"
  }
  vnet {
    // One of the arguments from this list "new_vnet existing_vnet" must be set
    existing_vnet {
      resource_group = var.vnet_resource_group_name
      vnet_name      = var.vnet_name
    }
  }
  // One of the arguments from this list "nodes_per_az total_nodes no_worker_nodes" must be set
  no_worker_nodes = true
  machine_type    = "Standard_D4a_v4"
}

resource "volterra_cloud_site_labels" "az_node_labels" {
  name      = volterra_azure_vnet_site.az_node.name
  site_type = "azure_vnet_site"
  labels = {
    "${var.prefix}.ce_site_selector" = var.ce_site_selector
  }
  ignore_on_delete = true
}


resource "volterra_tf_params_action" "az_node_tf_apply" {
  site_name       = volterra_azure_vnet_site.az_node.name
  site_kind       = "azure_vnet_site"
  action          = "apply"
  wait_for_action = true

  depends_on = [
    volterra_azure_vnet_site.az_node,
    var.timeout
  ]
}


resource "volterra_discovery" "aks_service_discovery" {
  count = var.create_k8s_service_discorvery == true ? 1 : 0

  name      = "${volterra_azure_vnet_site.az_node.name}-aks-service-discovery"
  namespace = "system"

  // One of the arguments from this list "discovery_k8s discovery_consul" must be set
  discovery_k8s {
    access_info {
      kubeconfig_url {
        clear_secret_info {
          url = "string:///${base64encode(var.kubeconfig_raw)}"
        }

        # blindfold_secret_info {
        #   location = "string:///${base64encode(var.kubeconfig_raw)}"
        # }

      }
    }
    publish_info {
      publish_fqdns = true
    }
  }

  where {
    // One of the arguments from this list "virtual_network site virtual_site" must be set

    site {
      network_type = "CUSTOMER_EDGE"
      ref {
        name      = volterra_azure_vnet_site.az_node.name
        namespace = "system"
      }
    }
  }

  depends_on = [
    volterra_tf_params_action.az_node_tf_apply
  ]
}
