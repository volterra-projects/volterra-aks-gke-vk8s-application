resource "azurerm_kubernetes_cluster" "aks_cluster" {
  name                = "${var.resource_prefix}-${var.cluster_name}"
  location            = var.aks_rg.location
  resource_group_name = var.aks_rg.name
  dns_prefix          = var.cluster_name
  node_resource_group = "${var.resource_prefix}-${var.cluster_name}-resources-rg"
  kubernetes_version  = data.azurerm_kubernetes_service_versions.current.latest_version

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = var.vm_size
    tags = {
      "Owner" = var.tag_owner
    }
    vnet_subnet_id = var.aks_vnet_subnet_inside_id
  }

  identity {
    type = "SystemAssigned"
  }

  azure_policy_enabled             = false
  http_application_routing_enabled = false


  tags = {
    "Owner" = var.tag_owner
  }
}

# Documentation Reference: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/kubernetes_service_versions
# Datasource to get Latest Azure AKS latest Version
data "azurerm_kubernetes_service_versions" "current" {
  location        = var.aks_rg.location
  version_prefix  = var.aks_version_prefix
  include_preview = false
}

data "azurerm_kubernetes_cluster" "aks_cluster" {
  name                = azurerm_kubernetes_cluster.aks_cluster.name
  resource_group_name = var.aks_rg.name
}

resource "local_file" "kubeconfig" {
  content  = data.azurerm_kubernetes_cluster.aks_cluster.kube_config_raw
  filename = "${abspath(path.root)}/creds/${azurerm_kubernetes_cluster.aks_cluster.name}-creds.yaml"
}
