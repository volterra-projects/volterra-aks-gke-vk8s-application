output "kubeconfig_path" {
  value = local_file.kubeconfig.filename
}

output "kubeconfig_raw" {
  value = data.azurerm_kubernetes_cluster.aks_cluster.kube_config_raw
}
