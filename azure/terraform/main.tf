terraform {
  required_version = ">= 0.13"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "aks_rg" {
  name     = "${var.resource_prefix}-${var.cluster_name}-rg"
  location = var.location
  tags = {
    "Owner" = var.tag_owner
  }
}

resource "azurerm_resource_group" "vm_rg" {
  name     = "${var.resource_prefix}-vm-rg"
  location = "eastus"
  tags = {
    "Owner" = var.tag_owner
  }
}

module "aks_vnet" {
  source              = "Azure/vnet/azurerm"
  vnet_name           = "${var.resource_prefix}-aks-vnet"
  resource_group_name = azurerm_resource_group.aks_rg.name
  address_space       = ["10.0.0.0/8"]
  subnet_prefixes     = ["10.240.0.0/16", "10.241.0.0/16"]
  subnet_names        = [var.subnet_inside_name, var.subnet_outside_name]

  tags = {
    "Owner" = var.tag_owner
  }

  depends_on = [azurerm_resource_group.aks_rg]
}

module "vm_vnet" {
  source              = "Azure/vnet/azurerm"
  vnet_name           = "${var.resource_prefix}-vm-vnet"
  resource_group_name = azurerm_resource_group.vm_rg.name
  address_space       = ["10.0.0.0/16"]
  subnet_prefixes     = ["10.0.1.0/24", "10.0.2.0/24"]
  subnet_names        = [var.subnet_inside_name, var.subnet_outside_name]

  tags = {
    "Owner" = var.tag_owner
  }

  depends_on = [azurerm_resource_group.vm_rg]

}

module "aks" {
  source = "./modules/aks"

  resource_prefix           = var.resource_prefix
  tag_owner                 = var.tag_owner
  cluster_name              = var.cluster_name
  aks_rg                    = azurerm_resource_group.aks_rg
  aks_vnet_subnet_inside_id = module.aks_vnet.vnet_subnets[0]
  vm_size                   = var.vm_size
  aks_version_prefix        = var.aks_version_prefix

  depends_on = [
    azurerm_resource_group.aks_rg,
    module.aks_vnet
  ]
}

module "linuxservers" {
  source              = "Azure/compute/azurerm"
  resource_group_name = azurerm_resource_group.vm_rg.name
  location            = azurerm_resource_group.vm_rg.location
  vm_hostname         = "dockerhost"
  vm_os_publisher     = "canonical"
  vm_os_sku           = "20_04-lts-gen2"
  vm_os_offer         = "0001-com-ubuntu-server-focal"
  vm_size             = "Standard_D2as_v5"

  nb_data_disk = 0
  nb_public_ip = 0
  nb_instances = 2

  delete_data_disks_on_termination = true
  delete_os_disk_on_termination    = true


  vnet_subnet_id = module.vm_vnet.vnet_subnets[0]

  custom_data = base64encode(local.custom_data)

  depends_on = [
    azurerm_resource_group.vm_rg,
    module.vm_vnet
  ]
}

locals {
  custom_data = <<CUSTOM_DATA
  #cloud-config
  groups:
    - docker
  users:
    - default
    # the docker service account
    - name: docker-service
      groups: docker
  package_upgrade: true
  packages:
    - apt-transport-https
    - ca-certificates
    - curl
    - gnupg-agent
    - software-properties-common
  runcmd:
    # install docker following the guide: https://docs.docker.com/install/linux/docker-ce/ubuntu/
    - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    - sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    - sudo apt-get -y update
    - sudo apt-get -y install docker-ce docker-ce-cli containerd.io
    - sudo systemctl enable docker
    - sudo docker run -d -p 80:8080 --restart unless-stopped registry.gitlab.com/sentence-app/adjectives:dev
  power_state:
    mode: reboot
    message: Restarting after installing docker
  CUSTOM_DATA
}


module "aks_voltmesh_node" {
  source = "./modules/volterra-az-node"

  api_cert_path = var.api_cert_path
  api_key_path  = var.api_key_path
  api_url       = var.api_url

  prefix = var.resource_prefix

  tag_owner = var.tag_owner

  location                 = azurerm_resource_group.aks_rg.location
  vnet_name                = module.aks_vnet.vnet_name
  vnet_resource_group_name = azurerm_resource_group.aks_rg.name
  vnet_subnet_inside_name  = var.subnet_inside_name

  ce_site_selector = "cloud-aks"

  create_k8s_service_discorvery = true
  kubeconfig_raw                = module.aks.kubeconfig_raw

  // Wait for nodes to destroy completely and detach from network
  timeout = time_sleep.wait_30_seconds
}

module "vm_voltmesh_node" {
  source = "./modules/volterra-az-node"

  api_cert_path = var.api_cert_path
  api_key_path  = var.api_key_path
  api_url       = var.api_url

  prefix = var.resource_prefix

  tag_owner = var.tag_owner

  location                 = azurerm_resource_group.vm_rg.location
  vnet_name                = module.vm_vnet.vnet_name
  vnet_resource_group_name = azurerm_resource_group.vm_rg.name
  vnet_subnet_inside_name  = var.subnet_inside_name

  ce_site_selector = "cloud-vm"

  // Wait for nodes to destroy completely and detach from network
  timeout = time_sleep.wait_30_seconds
}

module "kubectl-generator" {
  source  = "gyllenhammar/yaml-file/kubectl"
  version = "0.1.3"

  kubeconfig_path = module.aks.kubeconfig_path

  manifest_file_path = "./../k8s-deployments/aks-sentence-deployment.yaml"
}

module "kubectl-frontend" {
  source  = "gyllenhammar/yaml-file/kubectl"
  version = "0.1.3"

  kubeconfig_path = module.aks.kubeconfig_path

  manifest_file_path = "./../k8s-deployments/aks-sentence-deployment-nginx.yaml"
}

// Wait for nodes to destroy completely and detach from network
resource "time_sleep" "wait_30_seconds" {
  depends_on = [module.vm_vnet, module.aks_vnet]

  destroy_duration = "30s"
}
