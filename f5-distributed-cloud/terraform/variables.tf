variable "api_cert_path" {
  type        = string
  description = "Volterra API Cert path"
}

variable "api_key_path" {
  type        = string
  description = "Volterra API private key path"
}

variable "api_url" {
  type        = string
  description = "Volterra tenant api url"
}

variable "resource_prefix" {
  type        = string
  description = "Object Prefix"
}

variable "namespace" {
  type        = string
  description = "Volterra namespace to use"
}

variable "cred_expiry_days" {
  type        = number
  default     = 89
  description = "Days beforce vK8s kubeconfig credentials expire"
  validation {
    condition     = var.cred_expiry_days < 91
    error_message = "The maximum amount of days before credentials expire is 90."
  }
}

variable "sentence_ce_sto_site_selector" {
  type        = string
  description = "CE sto site selector"
}

variable "sentence_ce_mj_site_selector" {
  type        = string
  description = "CE mj site selector"
}

variable "sentence_ce_cloud_aks_site_selector" {
  type        = string
  description = "CE cloud site selector"
}

variable "sentence_ce_cloud_vm_site_selector" {
  type        = string
  description = "CE cloud site selector"
}

variable "sentence_ce_cloud_gke_site_selector" {
  type        = string
  description = "CE cloud site selector"
}

variable "sentence_re_site_selector" {
  type        = string
  description = "RE site selector"
}

variable "sentence_public_dns_name" {
  type        = string
  description = "sentence app public domain name, resource_prefix will be added"
  default     = "sentence.emea-ent.f5demos.com"
}
