//POOLS

resource "volterra_origin_pool" "sentence_colors_pool" {
  name                   = format("%s-colors", var.resource_prefix)
  namespace              = var.namespace
  description            = format("Origin pool pointing to colors service running in CE")
  loadbalancer_algorithm = "RANDOM"
  healthcheck {
    name      = volterra_healthcheck.sentence_colors_hc.name
    namespace = var.namespace
  }
  origin_servers {
    private_ip {
      ip              = "10.7.100.60"
      inside_network  = false
      outside_network = true
      site_locator {
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_sto.name
          namespace = var.namespace
        }
      }
    }
  }
  origin_servers {
    private_ip {
      ip              = "10.0.151.101"
      inside_network  = false
      outside_network = true
      site_locator {
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_mj.name
          namespace = var.namespace
        }
      }
    }
  }
  port               = 8080
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"

}

resource "volterra_origin_pool" "sentence_adjectives_pool" {
  name                   = format("%s-adjectives", var.resource_prefix)
  namespace              = var.namespace
  description            = format("Origin pool pointing to adjectives service running in azure CE")
  loadbalancer_algorithm = "RANDOM"
  healthcheck {
    name      = volterra_healthcheck.sentence_adjectives_hc.name
    namespace = var.namespace
  }
  origin_servers {
    private_name {
      dns_name        = "dockerhost-0.internal.cloudapp.net"
      inside_network  = false
      outside_network = true
      site_locator {
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_vm.name
          namespace = var.namespace
        }
      }
    }
  }
  origin_servers {
    private_name {
      dns_name        = "dockerhost-1.internal.cloudapp.net"
      inside_network  = false
      outside_network = true
      site_locator {
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_vm.name
          namespace = var.namespace
        }
      }
    }
  }
  port               = 80
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
}

# resource "volterra_origin_pool" "sentence_generator_pool" {
#   name                   = format("%s-generator", var.resource_prefix)
#   namespace              = var.namespace
#   description            = format("Origin pool pointing to generator service running in AKS on Azure CE")
#   loadbalancer_algorithm = "ROUND ROBIN"
#   healthcheck {
#     name      = volterra_healthcheck.sentence_generator_hc.name
#     namespace = var.namespace
#   }
#   origin_servers {
#     k8s_service {
#       inside_network  = true
#       outside_network = false
#       service_name    = "sentence-generator-nodeport.default"
#       site_locator {
#         virtual_site {
#           name      = volterra_virtual_site.sentence_ce_cloud_aks.name
#           namespace = var.namespace
#         }
#       }
#     }
#   }
#   port               = 80
#   no_tls             = true
#   endpoint_selection = "LOCAL_PREFERRED"
# }

resource "volterra_origin_pool" "sentence_frontend_aks_pool" {
  name                   = format("%s-frontend-aks", var.resource_prefix)
  namespace              = var.namespace
  description            = format("Origin pool pointing to frontend service running in AKS on Azure CE")
  loadbalancer_algorithm = "RANDOM"
  healthcheck {
    name      = volterra_healthcheck.sentence_frontend_hc.name
    namespace = var.namespace
  }
  origin_servers {
    k8s_service {
      inside_network  = false
      outside_network = true
      service_name    = "sentence-frontend-nginx-nodeport.default"
      site_locator {
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_aks.name
          namespace = var.namespace
        }
      }
    }
  }
  port               = 80
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
}

resource "volterra_origin_pool" "sentence_frontend_gke_pool" {
  name                   = format("%s-frontend-gke", var.resource_prefix)
  namespace              = var.namespace
  description            = format("Origin pool pointing to frontend service running in GKE on GKE-k8s CE")
  loadbalancer_algorithm = "RANDOM"
  healthcheck {
    name      = volterra_healthcheck.sentence_frontend_hc.name
    namespace = var.namespace
  }
  origin_servers {
    k8s_service {
      inside_network  = false
      outside_network = true
      service_name    = "sentence-frontend-nginx-nodeport.default"
      site_locator {
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_gke.name
          namespace = var.namespace
        }
      }
    }
  }
  port               = 80
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
}

# resource "volterra_origin_pool" "v8ks_sentence_frontend_pool" {
#   name                   = format("%s-vk8s-frontend", var.resource_prefix)
#   namespace              = var.namespace
#   description            = format("Origin pool pointing to frontend service running in vk8s on REs")
#   loadbalancer_algorithm = "ROUND ROBIN"
#   healthcheck {
#     name      = volterra_healthcheck.sentence_frontend_hc.name
#     namespace = var.namespace
#   }
#   origin_servers {
#     k8s_service {
#       vk8s_networks = true
#       service_name  = "sentence-frontend-nginx.a-gyllenhammar"
#       site_locator {
#         virtual_site {
#           name      = volterra_virtual_site.sentence_re.name
#           namespace = var.namespace
#         }
#       }
#     }
#   }
#   port               = 80
#   no_tls             = true
#   endpoint_selection = "LOCAL_PREFERRED"
# }
