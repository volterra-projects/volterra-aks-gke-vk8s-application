// HEALTHCHECKS

resource "volterra_healthcheck" "sentence_colors_hc" {
  name      = format("%s-colors-hc", var.resource_prefix)
  namespace = var.namespace

  http_health_check {
    use_origin_server_name = true
    path                   = "/colors"
  }
  healthy_threshold   = 3
  interval            = 15
  timeout             = 3
  unhealthy_threshold = 1
  jitter_percent      = 30
}

resource "volterra_healthcheck" "sentence_adjectives_hc" {
  name      = format("%s-adjectives-hc", var.resource_prefix)
  namespace = var.namespace

  http_health_check {
    use_origin_server_name = true
    path                   = "/adjectives"
  }
  healthy_threshold   = 3
  interval            = 15
  timeout             = 3
  unhealthy_threshold = 1
  jitter_percent      = 30
}

resource "volterra_healthcheck" "sentence_generator_hc" {
  name      = format("%s-generator-hc", var.resource_prefix)
  namespace = var.namespace

  http_health_check {
    use_origin_server_name = true
    path                   = "/health"
  }
  healthy_threshold   = 3
  interval            = 15
  timeout             = 3
  unhealthy_threshold = 1
  jitter_percent      = 30
}

resource "volterra_healthcheck" "sentence_frontend_hc" {
  name      = format("%s-frontend-hc", var.resource_prefix)
  namespace = var.namespace

  http_health_check {
    use_origin_server_name = true
    path                   = "/"
  }
  healthy_threshold   = 3
  interval            = 15
  timeout             = 3
  unhealthy_threshold = 1
  jitter_percent      = 30
}
