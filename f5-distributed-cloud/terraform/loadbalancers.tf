//LOADBALANCERS

locals {
  sentence_public_dns_name = format("%s-%s", var.resource_prefix, var.sentence_public_dns_name)
}

resource "volterra_http_loadbalancer" "sentence_colors_lb" {

  lifecycle {
    ignore_changes = [labels]
  }
  name        = format("%s-colors-lb", var.resource_prefix)
  namespace   = var.namespace
  description = format("Http loadbalancer object for %s sentence-colors service", var.resource_prefix)
  domains     = ["sentence-colors.default"]
  labels      = { "ves.io/app_type" : volterra_app_type.at.name }

  default_route_pools {
    pool {
      name      = volterra_origin_pool.sentence_colors_pool.name
      namespace = var.namespace
    }
    weight   = 1
    priority = 1
  }

  advertise_custom {
    advertise_where {
      virtual_site {
        network = "SITE_NETWORK_INSIDE_OUTSIDE"
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_gke.name
          namespace = var.namespace
        }
      }
      use_default_port = true
    }
    advertise_where {
      virtual_site {
        network = "SITE_NETWORK_OUTSIDE"
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_aks.name
          namespace = var.namespace
        }
      }
      use_default_port = true
    }
  }

  no_challenge = true
  round_robin  = true
  http {
    dns_volterra_managed = false
  }

  # single_lb_app {
  #   enable_discovery {
  #     disable_learn_from_redirect_traffic = true
  #   }
  #   disable_ddos_detection = true

  #   enable_malicious_user_detection = true

  # }

  multi_lb_app                    = true
  disable_rate_limit              = true
  service_policies_from_namespace = true
  disable_bot_defense             = true
  disable_waf                     = true
  user_id_client_ip               = true
}

resource "volterra_http_loadbalancer" "sentence_app_public_lb" {

  lifecycle {
    ignore_changes = [labels]
  }
  name        = format("%s-app-public-lb", var.resource_prefix)
  namespace   = var.namespace
  description = format("Public Http loadbalancer object for %s sentence-nginx service", var.resource_prefix)
  domains     = [local.sentence_public_dns_name]
  labels      = { "ves.io/app_type" : volterra_app_type.at.name }
  # depends_on = [
  #   time_sleep.vk8s_wait
  # ]

  default_route_pools {
    pool {
      name      = volterra_origin_pool.sentence_frontend_aks_pool.name
      namespace = var.namespace
    }
    weight   = 1
    priority = 1
  }

  default_route_pools {
    pool {
      name      = volterra_origin_pool.sentence_frontend_gke_pool.name
      namespace = var.namespace
    }
    weight   = 1
    priority = 1
  }

  advertise_on_public_default_vip = true

  no_challenge = true
  round_robin  = true

  https_auto_cert {
    http_redirect         = true
    no_mtls               = true
    add_hsts              = false
    default_header        = true
    enable_path_normalize = true
    tls_config {
      default_security = true
    }
  }

  # single_lb_app {
  #   enable_discovery {
  #     disable_learn_from_redirect_traffic = true
  #   }
  #   disable_ddos_detection = true

  #   enable_malicious_user_detection = true

  # }

  multi_lb_app = true

  disable_rate_limit              = true
  service_policies_from_namespace = true

  app_firewall {
    name      = volterra_app_firewall.sentence_waf.name
    namespace = "shared"
  }

  user_id_client_ip = true
}

resource "volterra_http_loadbalancer" "sentence_adjectives_lb" {

  lifecycle {
    ignore_changes = [labels]
  }
  name        = format("%s-adjectives-lb", var.resource_prefix)
  namespace   = var.namespace
  description = format("Http loadbalancer object for %s sentence-adjectives service", var.resource_prefix)
  domains     = ["sentence-adjectives.default"]
  labels      = { "ves.io/app_type" : volterra_app_type.at.name }

  default_route_pools {
    pool {
      name      = volterra_origin_pool.sentence_adjectives_pool.name
      namespace = var.namespace
    }
    weight   = 1
    priority = 1
  }

  advertise_custom {
    advertise_where {
      virtual_site {
        network = "SITE_NETWORK_OUTSIDE"
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_aks.name
          namespace = var.namespace
        }
      }
      use_default_port = true
    }
    advertise_where {
      virtual_site {
        network = "SITE_NETWORK_OUTSIDE"
        virtual_site {
          name      = volterra_virtual_site.sentence_ce_cloud_gke.name
          namespace = var.namespace
        }
      }
      use_default_port = true
    }
  }

  no_challenge = true
  round_robin  = true
  http {
    dns_volterra_managed = false
  }

  # single_lb_app {
  #   enable_discovery {
  #     disable_learn_from_redirect_traffic = true
  #   }
  #   disable_ddos_detection = true

  #   enable_malicious_user_detection = true

  # }

  multi_lb_app = true

  disable_rate_limit              = true
  service_policies_from_namespace = true

  disable_waf = true

  user_id_client_ip = true
}

# resource "volterra_http_loadbalancer" "sentence_generator_lb" {
#   lifecycle {
#     ignore_changes = [labels]
#   }
#   name        = format("%s-generator-lb", var.resource_prefix)
#   namespace   = var.namespace
#   description = format("Http loadbalancer object for %s sentence-generator service", var.resource_prefix)
#   domains     = ["sentence-generator.${var.namespace}"]
#   labels      = {}
#   depends_on = [
#     time_sleep.vk8s_wait
#   ]

#   default_route_pools {
#     pool {
#       name      = volterra_origin_pool.sentence_generator_pool.name
#       namespace = var.namespace
#     }
#     weight   = 1
#     priority = 1
#   }

#   advertise_custom {
#     advertise_where {
#       vk8s_service {
#         virtual_site {
#           name      = volterra_virtual_site.sentence_re.name
#           namespace = var.namespace
#         }
#       }
#       use_default_port = true
#     }
#   }

#   no_challenge = true
#   round_robin  = true
#   http {
#     dns_volterra_managed = false
#   }

#   # single_lb_app {
#   #   enable_discovery {
#   #     disable_learn_from_redirect_traffic = true
#   #   }
#   #   disable_ddos_detection = true

#   #   enable_malicious_user_detection = true

#   # }

#   multi_lb_app = true

#   disable_rate_limit              = true
#   service_policies_from_namespace = true

#   disable_waf = true

#   user_id_client_ip = true
# }
