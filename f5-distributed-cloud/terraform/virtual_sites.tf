//VIRTUAL SITES

locals {
  sentence_re_site_selector           = ["${var.sentence_re_site_selector}"]
  sentence_ce_sto_site_selector       = ["agy.${var.sentence_ce_sto_site_selector}"]
  sentence_ce_mj_site_selector        = ["agy.${var.sentence_ce_mj_site_selector}"]
  sentence_ce_cloud_aks_site_selector = ["${var.resource_prefix}.${var.sentence_ce_cloud_aks_site_selector}"]
  sentence_ce_cloud_vm_site_selector  = ["${var.resource_prefix}.${var.sentence_ce_cloud_vm_site_selector}"]
  sentence_ce_cloud_gke_site_selector = ["${var.resource_prefix}.${var.sentence_ce_cloud_gke_site_selector}"]
}

resource "volterra_virtual_site" "sentence_ce_sto" {
  name      = format("%s-ce-sto-vs", var.resource_prefix)
  namespace = var.namespace

  site_selector {
    expressions = local.sentence_ce_sto_site_selector
  }
  site_type = "CUSTOMER_EDGE"
}

resource "volterra_virtual_site" "sentence_ce_mj" {
  name      = format("%s-ce-mj-vs", var.resource_prefix)
  namespace = var.namespace

  site_selector {
    expressions = local.sentence_ce_mj_site_selector
  }
  site_type = "CUSTOMER_EDGE"
}

resource "volterra_virtual_site" "sentence_ce_cloud_aks" {
  name      = format("%s-ce-cloud-aks-vs", var.resource_prefix)
  namespace = var.namespace

  site_selector {
    expressions = local.sentence_ce_cloud_aks_site_selector
  }
  site_type = "CUSTOMER_EDGE"

}

resource "volterra_virtual_site" "sentence_ce_cloud_vm" {
  name      = format("%s-ce-cloud-vm-vs", var.resource_prefix)
  namespace = var.namespace

  site_selector {
    expressions = local.sentence_ce_cloud_vm_site_selector
  }
  site_type = "CUSTOMER_EDGE"

}

resource "volterra_virtual_site" "sentence_ce_cloud_gke" {
  name      = format("%s-ce-cloud-gke-vs", var.resource_prefix)
  namespace = var.namespace

  site_selector {
    expressions = local.sentence_ce_cloud_gke_site_selector
  }
  site_type = "CUSTOMER_EDGE"

}

resource "volterra_virtual_site" "sentence_re" {
  name      = format("%s-re-vs", var.resource_prefix)
  namespace = var.namespace

  site_selector {
    expressions = local.sentence_re_site_selector
  }
  site_type = "REGIONAL_EDGE"
}

resource "volterra_virtual_site" "sentence_loadgen_re" {
  name      = format("%s-loadgen-re-vs", var.resource_prefix)
  namespace = var.namespace

  site_selector {
    expressions = ["ves.io/region in (ves-io-amsterdam, ves-io-newyork)"]
  }
  site_type = "REGIONAL_EDGE"
}
