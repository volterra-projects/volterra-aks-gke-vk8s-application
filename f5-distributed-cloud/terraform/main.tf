terraform {
  required_version = ">= 0.13"
  required_providers {
    volterra = {
      source  = "volterraedge/volterra"
      version = "~> 0.11.0"
    }
  }
}

provider "volterra" {
  api_cert = var.api_cert_path
  api_key  = var.api_key_path
  url      = var.api_url

  timeout = "120s"
}

// AI & ML

resource "volterra_app_type" "at" {
  // This naming simplifies the 'mesh' cards
  name      = "${var.resource_prefix}-app-type"
  namespace = "shared"
  features {
    type = "BUSINESS_LOGIC_MARKUP"
  }
  features {
    type = "USER_BEHAVIOR_ANALYSIS"
  }
  features {
    type = "PER_REQ_ANOMALY_DETECTION"
  }
  features {
    type = "TIMESERIES_ANOMALY_DETECTION"
  }
  business_logic_markup_setting {
    enable = true
  }
}

resource "volterra_app_setting" "as" {
  name      = "${var.resource_prefix}-app-settings"
  namespace = var.namespace

  app_type_settings {
    app_type_ref {
      name      = volterra_app_type.at.name
      namespace = volterra_app_type.at.namespace
    }
    business_logic_markup_setting {
      enable = true
    }
    timeseries_analyses_setting {
      metric_selectors {
        metric         = ["REQUEST_RATE", "ERROR_RATE", "LATENCY", "THROUGHPUT"]
        metrics_source = "NODES"
      }
    }
    user_behavior_analysis_setting {
      enable_learning = true
      enable_detection {
        cooling_off_period = 20
        include_forbidden_activity {
          forbidden_requests_threshold = 10
        }
        exclude_failed_login_activity = true
        include_waf_activity          = true
      }
    }
  }
}

// LOADGENERATOR

module "loadgen-container" {
  source          = "gyllenhammar/yaml-file/kubectl"
  version         = "0.1.3"
  kubeconfig_path = local_file.kubeconfig.filename

  manifest_content = templatefile(
    "${path.root}/../k8s-deployments/templates/loadgen-manifest-template.tftpl",
    { namespace  = var.namespace,
      vsite      = volterra_virtual_site.sentence_loadgen_re.name,
      reg_server = "registry.gitlab.com/volterra-projects/volterra-aks-gke-vk8s-application",
      image_name = "loadtest",
      image_tag  = "latest",
      target_url = format("https://%s", volterra_http_loadbalancer.sentence_app_public_lb.domains.0)
  })
}
