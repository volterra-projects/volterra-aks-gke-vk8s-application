terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~>4.0"
    }
    volterra = {
      source  = "volterraedge/volterra"
      version = "~> 0.11.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.0"
    }
  }

  required_version = ">= 0.13"
}

provider "google" {
  project = var.project_id
  region  = var.region
}

module "kubectl-generator" {
  source  = "gyllenhammar/yaml-file/kubectl"
  version = "0.1.3"

  kubeconfig_path = module.kubeconfig.kubeconfig_path

  manifest_file_path = "./../k8s-deployments/aks-sentence-deployment.yaml"
}

module "kubectl-frontend" {
  source  = "gyllenhammar/yaml-file/kubectl"
  version = "0.1.3"

  kubeconfig_path = module.kubeconfig.kubeconfig_path

  manifest_file_path = "./../k8s-deployments/aks-sentence-deployment-nginx.yaml"
}

# module "kubectl-volterra-node" {
#   source = "./modules/kubectl"

#   kubecfg = local.kubeconfig_file

#   manifest_file_path = templatefile("${path.root}/../k8s-deployments/templates/volterra-k8s-node-template.tftpl", { cluster_name = "${var.resource_prefix}-${var.cluster_name}", volterra_token = var.volterra_token })

#   dependency_var = {}
# }
