# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {
  depends_on = [google_container_cluster.primary]
}

data "google_container_cluster" "primary" {
  name       = google_container_cluster.primary.name
  location   = google_container_cluster.primary.location
  depends_on = [google_container_cluster.primary]
}

data "google_container_engine_versions" "cluster_version" {
  project        = var.project_id
  location       = "${var.region}-b"
  version_prefix = "1.21."
}

resource "google_container_cluster" "primary" {
  name               = "${var.resource_prefix}-${var.cluster_name}"
  location           = "${var.region}-b"
  project            = var.project_id
  initial_node_count = var.gke_num_nodes



  network    = google_compute_network.vpc.id
  subnetwork = google_compute_subnetwork.subnet.id

  ip_allocation_policy {
    cluster_secondary_range_name  = "${var.resource_prefix}-${var.project_id}-cluster-services-range"
    services_secondary_range_name = google_compute_subnetwork.subnet.secondary_ip_range.1.range_name
  }

  min_master_version = data.google_container_engine_versions.cluster_version.latest_master_version

  node_config {
    machine_type = var.machine_type
  }

  timeouts {
    create = "30m"
    update = "40m"
  }
}
