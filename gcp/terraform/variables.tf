variable "resource_prefix" {
  description = "Resource prefix"
}

variable "project_id" {
  description = "project id"
}

variable "region" {
  default     = "us-east1"
  description = "region"
}

variable "cluster_name" {
  default     = "gke-cluster"
  description = "cluster name"
}

variable "volterra_token" {
  type = string
}

variable "gke_num_nodes" {
  default     = 1
  description = "number of nodes per zone"
}

variable "machine_type" {
  default     = "e2-standard-2"
  description = "node machine type"
}

variable "api_cert_path" {
  type        = string
  description = "Volterra API Cert path"
}

variable "api_key_path" {
  type        = string
  description = "Volterra API private key path"
}

variable "api_url" {
  type        = string
  description = "Volterra tenant api url"
}


