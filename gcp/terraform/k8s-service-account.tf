provider "kubernetes" {
  host  = "https://${data.google_container_cluster.primary.endpoint}"
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate,
  )
}

locals {
  cluster_name         = "${var.resource_prefix}-gke-cluster"
  service_account_name = "sentence-sa"
}

resource "kubernetes_service_account" "gke_sa" {
  metadata {
    name      = local.service_account_name
    namespace = "default"
  }
  depends_on = [
    data.google_client_config.provider
  ]
}

data "kubernetes_service_account" "gke_sa" {
  metadata {
    name      = kubernetes_service_account.gke_sa.metadata.0.name
    namespace = kubernetes_service_account.gke_sa.metadata.0.namespace
  }
}

data "kubernetes_secret" "gke_sa_secret" {
  metadata {
    name = data.kubernetes_service_account.gke_sa.default_secret_name
  }
}

resource "kubernetes_cluster_role_binding" "gke_sa_crb" {
  metadata {
    name = "terraform-example"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gke_sa.metadata.0.name
    namespace = "default"
  }
}

module "kubeconfig" {
  source  = "redeux/kubeconfig/kubernetes"
  version = "0.0.2"

  filename = "creds/gke_kubeconfig.yaml"
  clusters = [{
    "name" : local.cluster_name,
    "server" : format("https://%s", data.google_container_cluster.primary.endpoint),
    "certificate_authority_data" = data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate
  }]
  contexts = [{
    "name" : local.cluster_name,
    "cluster_name" : local.cluster_name,
    "user" : kubernetes_service_account.gke_sa.metadata.0.name
  }]
  current_context = local.cluster_name
  users = [{
    "name" : kubernetes_service_account.gke_sa.metadata.0.name,
    "token" : data.kubernetes_secret.gke_sa_secret.data["token"]
  }]

  depends_on = [
    kubernetes_cluster_role_binding.gke_sa_crb
  ]
}

data "local_file" "kubeconf" {
  filename = module.kubeconfig.kubeconfig_path
  depends_on = [
    module.kubeconfig
  ]
}
