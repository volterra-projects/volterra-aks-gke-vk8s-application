provider "volterra" {
  api_cert = var.api_cert_path
  api_key  = var.api_key_path
  url      = var.api_url
}

locals {
  site_name    = "${var.resource_prefix}-${var.cluster_name}"
  cluster_size = 1
  hostname     = "vp-manager-0"
  namespace    = "system"

  labels = {
    "${var.resource_prefix}.ce_site_selector" = "cloud-gke"
  }

  ce_site_selector = "cloud-gcp"
}

resource "volterra_gcp_vpc_site" "gcp_site" {
  lifecycle {
    ignore_changes = [
      labels
    ]
  }

  name        = format("%s-gcp-site", var.resource_prefix)
  namespace   = "system"
  description = format("%s site ", local.site_name)

  cloud_credentials {
    name      = "a-gyllenhammar-gcp"
    namespace = "system"
  }

  # MEmes - this demo breaks if assisted mode is used;
  assisted = false

  gcp_region              = var.region
  instance_type           = "n1-standard-4"
  logs_streaming_disabled = true

  ingress_gw {
    gcp_certified_hw = "gcp-byol-voltmesh"

    gcp_zone_names = ["us-east1-b"]

    local_network {

      existing_network {
        name = google_compute_network.vpc.name
      }
    }

    local_subnet {
      existing_subnet {
        subnet_name = google_compute_subnetwork.subnet.name
      }
    }
    node_number = 1
  }
}

resource "volterra_cloud_site_labels" "gcp_node_labels" {
  name             = volterra_gcp_vpc_site.gcp_site.name
  site_type        = "gcp_vpc_site"
  labels           = local.labels
  ignore_on_delete = true
}

# Instruct Volterra to provision the GCP VPC sites
resource "volterra_tf_params_action" "gcp_site_tf_apply" {
  site_name        = volterra_gcp_vpc_site.gcp_site.name
  site_kind        = "gcp_vpc_site"
  action           = "apply"
  wait_for_action  = true
  ignore_on_update = false
  # These shouldn't be necessary, but lifecycle is flaky without them
  # depends_on = [module.inside, module.outside, volterra_gcp_vpc_site.inside]
}

resource "volterra_discovery" "gke_service_discovery" {

  name      = "${volterra_gcp_vpc_site.gcp_site.name}-gke-service-discovery"
  namespace = "system"

  // One of the arguments from this list "discovery_k8s discovery_consul" must be set
  discovery_k8s {
    access_info {
      kubeconfig_url {
        clear_secret_info {
          url = format("string:///%s", base64encode(data.local_file.kubeconf.content))
        }
      }
    }
    publish_info {
      publish_fqdns = true
    }
  }

  where {
    // One of the arguments from this list "virtual_network site virtual_site" must be set
    site {
      network_type = "CUSTOMER_EDGE"
      ref {
        name      = volterra_gcp_vpc_site.gcp_site.name
        namespace = "system"
      }
    }
  }

  depends_on = [
    volterra_tf_params_action.gcp_site_tf_apply,
    module.kubeconfig
  ]
}
